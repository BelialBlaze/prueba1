package com.example.duoc.martinramirez_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {
    private Button btnCrearUsuario, btnVolverLogin;
    private EditText txtUsuario, txtClave,txtRepetirClave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        txtUsuario=(EditText)findViewById(R.id.txtUsuario);
        txtClave=(EditText)findViewById(R.id.txtClave);
        txtRepetirClave=(EditText)findViewById(R.id.txtRepetirClave);
        btnCrearUsuario=(Button)findViewById(R.id.btnCrearUsuario);
        btnVolverLogin=(Button)findViewById(R.id.btnVolverLogin);



                btnCrearUsuario.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        validarFormulario();
                    }

                    private void validarFormulario() {
                        String mensajeError="";
                        if (txtUsuario.getText().toString().length()<1){

                            mensajeError+="Ingrese Usuario \n";
                        }
                        if (txtClave.getText().toString().length()<4 && txtClave.getText().toString().length()>20)
                        {
                            mensajeError+="La clave deve de tener minimo 4 caracteres y maximo 20 caracteres\n";
                        }
                        if (txtRepetirClave.getText().toString() != txtClave.getText().toString())
                        {
                            mensajeError+="Clave incorrecta\n";
                        }
                        else
                        {
                            Toast.makeText(RegistroActivity.this, "Usuario  Creado", Toast.LENGTH_LONG).show();
                        }

                    }

                });

            btnVolverLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
                    startActivity(i);
                }
            });
    }
}
