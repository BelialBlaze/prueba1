package com.example.duoc.martinramirez_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private Button btnEntrar, btnRegistro;
    private EditText txtUsuario, txtClave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       txtUsuario=(EditText)findViewById(R.id.txtUsuario);
       txtClave=(EditText)findViewById(R.id.txtClave);
        btnEntrar=(Button)findViewById(R.id.btnEntrar);
        btnRegistro=(Button)findViewById(R.id.btnRegistro);





        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.btnEntrar) {

                    if (txtClave.getText().toString().equals("1234") &&
                    txtUsuario.getText().toString().equals("asdf")) {
                        Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_LONG).show();

                    } else {

                        Toast.makeText(LoginActivity.this, "Usuario no valido", Toast.LENGTH_LONG).show();

                    }

                }else if (v.getId() == R.id.btnRegistro){
                    Intent i = new Intent(LoginActivity.this, ListadoUsuariosActivity.class);
                    startActivity(i);
                }
            }
            });


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });



    }
}
