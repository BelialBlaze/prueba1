package com.example.duoc.martinramirez_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListadoUsuariosActivity extends AppCompatActivity {
    private ListView lvUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);

        final Usuario[] datos =
                new Usuario[]{};

        ArrayAdapter<Usuario> adaptador =
                new ArrayAdapter<Usuario>(this,
                        android.R.layout.simple_list_item_1, datos);

        lvUsuarios = (ListView)findViewById(R.id.lvUsuarios);

        lvUsuarios.setAdapter(adaptador);
}}
